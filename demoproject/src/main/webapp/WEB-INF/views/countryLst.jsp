<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>
<c:set var="req" value="${pageContext.request.contextPath}" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="${req}/resources/js/jquery-1.12.0.js"></script>
<script type="text/javascript">
$(document).ready(function() {	
	
	/*---*/		
	  $(".remove").on('click',function(event) { 
    	   //tableToBeRemoved = $(this).closest('table').parent().closest('table');
    	  // console.log($(this).closest('div').attr('id'));  
    	  // console.log($(this).parent().attr('id')); 
    	   
    	   $.ajax({
    			type : "POST",
    			url : "remove",
    			data : "id="+ $(this).parent().attr('id'),
    			success : function(data) {
    				var rowAffected = $.parseJSON(data);
    				if (rowAffected != null) {
    					console.log("Row deleted count: " + rowAffected); 
    				} else {}								

    			},
    			error : function() {
    				 console.log("ERROR: Removing data error");										
    			}
    		}); 
    	  
    }); 	
	
	
	/*---*/	
	// $('div#7').children("input").not(":button[type=button]")
	  $(".edit").on('click',function(event) {    		  
		  
		 var country2Update = {id:+ $(this).parent().attr('id')};
	    $.each($(this).parent().children("input").not(":button[type=button]"),
		      function( index, value ) { country2Update[$(this).attr('name')]=$(this).val()});
	  		  
		  $.ajax({
			type : "POST",
			url : "edit",
			data : JSON.stringify(country2Update),
			dataType: 'json', 
			contentType: 'application/json',
		    mimeType: 'application/json',
			success : function(data) {				
				if (data != null) {
					console.log(data);
				} else {}								

			},
			error : function() {
				 console.log("ERROR: Removing data error");										
			}
		});
		
   }); 
		
	/*---*/
	
});
</script>
<title>CRUD ops</title>
</head>
<body>
	<a href="add">Add New Country</a><br/>
	<br />

<c:forEach items="${Lst}" var="country" varStatus="status">	
<div id="${country.id}">
  <input name="name" value="${country.name}" />		
  <input name="a2" value="${country.a2}" />			
  <input name="a3" value="${country.a3}" />
  <input name="displayOrder" value="${country.displayOrder}" />
  <input name="numericCode" value="${country.numericCode}" /> 
  <input type="button" value="Remove" name="remove" class="remove"/>
  <input type="button" value="Edit"   name="edit" class="edit"/>
</div>  
  <br />  
</c:forEach>

</body>
</html>