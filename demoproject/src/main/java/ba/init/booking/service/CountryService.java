package ba.init.booking.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ba.init.booking.dao.ICountry;
import ba.init.booking.dto.CountryDTO;
import ba.init.booking.model.Country;

@Transactional
@Service("countryService")
public class CountryService {
	
	@Autowired
	ICountry countryDao;
	
	
	@Transactional(readOnly= true)
	public List<Country> findAllCountries(){
		return countryDao.findAllCountries();
	}
	
	@Transactional
	public void removeCountry(int id){
		Country country = countryDao.findById(id);
		countryDao.remove(country);
	}
	
	public void addCountry(CountryDTO countryDTO) {
		Country country = countryDTO2Country(countryDTO);
		countryDao.persist(country);
	}
	

	public Country editCountry(CountryDTO countryDTO) {		
		Country country = countryDTO2Country(countryDTO);
		return countryDao.update(country);		
	}

	public int deleteCountry(int id) {		
		return countryDao.removeById(id);
	}

	private Country countryDTO2Country(CountryDTO countryDTO) {
		Country country = new Country();
		
		if(countryDTO.getId() != null){
			country.setId(countryDTO.getId());
		}
		
		country.setName(countryDTO.getName());
		country.setA2(countryDTO.getA2());
		country.setA3(countryDTO.getA3());
		country.setDisplayOrder(countryDTO.getDisplayOrder());
		country.setNumericCode(countryDTO.getNumericCode());
		country.setCreated(new Date());
		country.setCreatedBy("alaa");
		return country;
	}


}
