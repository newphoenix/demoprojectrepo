package ba.init.booking.dao;

import java.util.List;

import ba.init.booking.model.Country;

public interface ICountry {

	void persist(Country transientInstance);

	void remove(Country persistentInstance);

	Country merge(Country detachedInstance);

	Country findById(Integer id);

	List<Country> findAllCountries();
	
	int removeById(int id);

	Country update(Country country);

}