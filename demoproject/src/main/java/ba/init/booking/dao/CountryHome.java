package ba.init.booking.dao;
// Generated Feb 25, 2016 9:56:11 AM by Hibernate Tools 4.3.1.Final

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import ba.init.booking.model.Country;

/**
 * Home object for domain model class Country.
 * @see ba.init.booking.model.Country
 * @author Hibernate Tools
 */
@Repository
public class CountryHome implements ICountry {

	private static final Log log = LogFactory.getLog(CountryHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see ba.init.booking.dao.ICountry#persist(ba.init.booking.model.Country)
	 */
	@Override
	public void persist(Country transientInstance) {
		log.debug("persisting Country instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see ba.init.booking.dao.ICountry#remove(ba.init.booking.model.Country)
	 */
	@Override
	public void remove(Country persistentInstance) {
		log.debug("removing Country instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see ba.init.booking.dao.ICountry#merge(ba.init.booking.model.Country)
	 */
	/* (non-Javadoc)
	 * @see ba.init.booking.dao.ICountryShort#merge(ba.init.booking.model.Country)
	 */
	@Override
	public Country merge(Country detachedInstance) {
		log.debug("merging Country instance");
		try {
			Country result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see ba.init.booking.dao.ICountry#findById(java.lang.Integer)
	 */
	/* (non-Javadoc)
	 * @see ba.init.booking.dao.ICountryShort#findById(java.lang.Integer)
	 */
	@Override
	public Country findById(Integer id) {
		log.debug("getting Country instance with id: " + id);
		try {
			Country instance = entityManager.find(Country.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@Override
	public List<Country> findAllCountries() {	
		return entityManager.createQuery("SELECT c FROM Country c ", Country.class).getResultList();
	}

	@Override
	public int removeById(int id) {
		log.debug("deleting Country instance with id: " + id);
		Query query = entityManager.createQuery("DELETE FROM Country c WHERE c.id = :id");
		return query.setParameter("id", id).executeUpdate();		
	}

	@Override
	public Country update(Country country) {
		log.debug("updating country: " + country);
		Country countryUpdated  = entityManager.merge(country);
		return countryUpdated ;
	}
}
