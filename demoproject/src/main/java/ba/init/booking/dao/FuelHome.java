package ba.init.booking.dao;
// Generated Feb 24, 2016 10:13:59 AM by Hibernate Tools 4.3.1.Final

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import ba.init.booking.model.Fuel;

/**
 * Home object for domain model class Fuel.
 * @see ba.init.booking.model.Fuel
 * @author Hibernate Tools
 */
@Repository
public class FuelHome {

	private static final Log log = LogFactory.getLog(FuelHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	public void persist(Fuel transientInstance) {
		log.debug("persisting Fuel instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void remove(Fuel persistentInstance) {
		log.debug("removing Fuel instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	public Fuel merge(Fuel detachedInstance) {
		log.debug("merging Fuel instance");
		try {
			Fuel result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Fuel findById(Integer id) {
		log.debug("getting Fuel instance with id: " + id);
		try {
			Fuel instance = entityManager.find(Fuel.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
