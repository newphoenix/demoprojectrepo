package ba.init.booking;

import java.util.Locale;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ba.init.booking.dto.CountryDTO;
import ba.init.booking.model.Country;
import ba.init.booking.service.CountryService;

/**
 * Handles requests for the application home page.
 */
@Controller
@RequestMapping(value = "/country")
public class CountryController {
	
	private static final Logger logger = LoggerFactory.getLogger(CountryController.class);
	
	@Resource(name="countryService")
	CountryService countryService;
	

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {	
	
		model.addAttribute("countryDTO", new CountryDTO());
		model.addAttribute("Lst", countryService.findAllCountries() );
		
		return "countryLst";
	}
	
	
	
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addFormCountry(Locale locale, Model model,CountryDTO countryDTO) {					
		model.addAttribute("countryDTO", new CountryDTO());		
		return "addCountry";
	}	
	
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addCountry(Locale locale, Model model,CountryDTO countryDTO) {		
		countryService.addCountry(countryDTO);		
		model.addAttribute("Lst", countryService.findAllCountries() );		
		return "countryLst";
	}
		
	@RequestMapping(value = "/remove", method = RequestMethod.POST)
	public @ResponseBody int removeCountry(Locale locale, Model model,int id) {
		
		int rowCount = countryService.deleteCountry(id);				
		return rowCount;
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public @ResponseBody Country editCountry(Locale locale, Model model, @RequestBody CountryDTO countryDTO) {
		return countryService.editCountry(countryDTO);	
	}
	
	
}
