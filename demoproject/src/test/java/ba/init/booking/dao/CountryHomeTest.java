package ba.init.booking.dao;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ba.init.booking.model.Country;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("file:src/main/webapp/WEB-INF/spring/root-context.xml")
public class CountryHomeTest {
	
	@PersistenceContext(name="emf")
	private EntityManager entityManager;
	

	@Before
	public void setUp() throws Exception {
	}

	@Ignore
	@Test
	@Transactional(propagation= Propagation.REQUIRED)
	public void test() {
		
		
		
		Country country = new Country();
				
		country.setName("AAAAA");
		country.setA2("AA");
		country.setA3("AAA");
		country.setNumericCode(1001007);
		country.setCreated(new Date());
		country.setCreatedBy("MMX");
		country.setDisplayOrder(50001);
			

		
		//entityManager.persist(country);
				
	}

}
